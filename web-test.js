let connectButton = document.getElementById('connectButton');
let phantomGetButton = document.getElementById('phantomGetButton');
let phantomOnButton = document.getElementById('phantomOnButton');
let phantomOffButton = document.getElementById('phantomOffButton');
let infoText = document.getElementById('infoText')

let hidParams = { filters: [{ vendorId: 0x14ED, productId: 0x1013 }] };

let reportId = 0x01;
let buffer = new Uint8Array(64);

let mvDevice;

function showReport(e) {
  arr = new Uint8Array(e.data.buffer);
  const nums = [];
  arr.forEach(v => nums.push(v.toString(16).padStart(2, '0')));
  infoText.innerHTML += 'Report! (' + nums.join('') + ')<br>';
};

function getPhantomState()
{
  buffer[0]  = 0x10; buffer[1]  = 0x11; buffer[2]  = 0x22; buffer[3]  = 0x06; buffer[4]  = 0x03; buffer[5]  = 0x08;
  buffer[6]  = 0x08; buffer[7]  = 0x70; buffer[8]  = 0x08; buffer[9]  = 0x01; buffer[10] = 0x02; buffer[11] = 0x02;
  buffer[12] = 0x00; buffer[13] = 0x01; buffer[14] = 0x66; buffer[15] = 0xa2; buffer[16] = 0xfe; buffer[17] = 0x00;

  mvDevice.sendReport(reportId, buffer.slice(0,63));
}

const delay = ms => new Promise(res => setTimeout(res, ms));

async function setFeatPhantomOff()
{
  buffer[0]  = 0x11; buffer[1]  = 0x11; buffer[2]  = 0x22; buffer[3]  = 0x94; buffer[4]  = 0x00; buffer[5]  = 0x08;
  buffer[6]  = 0x09; buffer[7]  = 0x70; buffer[8]  = 0x09; buffer[9]  = 0x02; buffer[10] = 0x02; buffer[11] = 0x02;
  buffer[12] = 0x00; buffer[13] = 0x01; buffer[14] = 0x66; buffer[15] = 0x00; buffer[16] = 0x9e; buffer[17] = 0x42;
  mvDevice.sendReport(reportId, buffer.slice(0,63));

  await delay(10);

  buffer[0]  = 0x0d; buffer[1]  = 0x11; buffer[2]  = 0x22; buffer[3]  = 0x08; buffer[4]  = 0x03; buffer[5]  = 0x08;
  buffer[6]  = 0x05; buffer[7]  = 0x70; buffer[8]  = 0x05; buffer[9]  = 0x01; buffer[10] = 0x00; buffer[11] = 0x00;
  buffer[12] = 0x1b; buffer[13] = 0x4e; buffer[14] = 0x00; buffer[15] = 0x00; buffer[16] = 0x00; buffer[17] = 0x00;
  mvDevice.sendReport(reportId, buffer.slice(0,63));
}

async function setFeatPhantomOn()
{
  buffer[0]  = 0x11; buffer[1]  = 0x11; buffer[2]  = 0x22; buffer[3]  = 0x07; buffer[4]  = 0x00; buffer[5]  = 0x08;
  buffer[6]  = 0x09; buffer[7]  = 0x70; buffer[8]  = 0x09; buffer[9]  = 0x02; buffer[10] = 0x02; buffer[11] = 0x02;
  buffer[12] = 0x00; buffer[13] = 0x01; buffer[14] = 0x66; buffer[15] = 0x30; buffer[16] = 0x18; buffer[17] = 0x86;
  mvDevice.sendReport(reportId, buffer.slice(0,63));

  await delay(10);

  buffer[0]  = 0x0d; buffer[1]  = 0x11; buffer[2]  = 0x22; buffer[3]  = 0x08; buffer[4]  = 0x03; buffer[5]  = 0x08;
  buffer[6]  = 0x05; buffer[7]  = 0x70; buffer[8]  = 0x05; buffer[9]  = 0x01; buffer[10] = 0x00; buffer[11] = 0x00;
  buffer[12] = 0x1b; buffer[13] = 0x4e; buffer[14] = 0x00; buffer[15] = 0x00; buffer[16] = 0x00; buffer[17] = 0x00;
  mvDevice.sendReport(reportId, buffer.slice(0,63));
}

async function mvConnect() {
  let deviceList = await navigator.hid.requestDevice(hidParams);
  if (deviceList === null || deviceList.length == 0)  { return; }

  mvDevice = deviceList[0];

  console.log('Name: ' + mvDevice.productName);
  if (mvDevice.open()) {
    console.log('Opened. Starting listener.');
    mvDevice.addEventListener('inputreport', showReport);
  };
};

connectButton.addEventListener('click', mvConnect);
phantomGetButton.addEventListener('click', getPhantomState);
phantomOnButton.addEventListener('click', setFeatPhantomOn);
phantomOffButton.addEventListener('click', setFeatPhantomOff);
