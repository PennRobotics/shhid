Shhid
=====
_What about that shhid?!_

**Shhid** is **Shure (MOTIV) using WebHID**: a MVX2U mic adapter client.


## Quick Start

Plug in the adapter. Make sure it has proper permissions to be accessed by a
Chrome-based browser. Open the browser and navigate to the local clone of the
HTML page.

For setting device permissions in Linux:

```sh
echo 'SUBSYSTEM=="hidraw", ATTRS{idVendor}=="14ed", ATTRS{idProduct}=="1013", MODE="0666"\nSUBSYSTEM=="usb", ATTRS{idVendor}=="14ed", ATTRS{idProduct}=="1013", MODE="0666"' | sudo tee /etc/udev/rules.d/62-shure-mvx2u.rules
sudo udevadm control --reload-rules && sudo udevadm trigger
```

Help this project succeed!
**Please report the first place where friction is found while using this project.**


## Details

Firefox and Safari do not support WebHID. It must be enabled with a flag in a
Chrome-based browser (including Edge). On most Linux PCs, you will need to add a
udev rule permitting user access with its subsystem set to hidraw. On a remote
server, Secure HTTP will need to be enabled. For details, check the **.html** or
**.js** files in this repository.

Once you have all the prequisites fulfilled, you can open **web-test.html** and
send test commands to the microphone adapter.

There is a proof-of-concept in **gui.html**. The plan is to closely mimic the
interface of _MOTIV Desktop_, although currently the GUI does not have events
programmed and there is no packet information for sending to and from the adapter.

In short, the web GUI is not ready yet. However, quite a few loose ends are here.


## License

**Shhid** is an [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)-licensed
project. Original source is at https://gitlab.com/PennRobotics/shhid/ and the
following guidelines shall apply:

1. The original copyright notice must be included without changes to the notice
2. Include the full Apache 2.0 license text with the code.
3. State significant changes made to the code.

Afterward, feel free to&hellip;

* change the original or any derived code
* distribute the original or any derivative
* charge money for the source and/or a binary
* keep the source private
* mix this code with compatibly licensed works

```text
   Copyright 2024 Brian Wright

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
